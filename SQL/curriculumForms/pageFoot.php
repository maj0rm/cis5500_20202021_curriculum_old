<?php

/**
 * pageFoot.php
 *
 * Footer for each page that closes tags opened by the header
 *
 * @author twhiten
 * @since 20201/03/03
 */


    $db->close();
?>

</div>
</body>
</html>
