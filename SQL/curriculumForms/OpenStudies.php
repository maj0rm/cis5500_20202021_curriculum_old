<?php

/**
 * OpenStudies.php
 *
 * Return a list of "open study" courses
 *
 * @author twhiten
 * @since 20201/03/09
 */

    include('pageHead.php');


    $query = 'SELECT CourseId, CourseTitle FROM CourseCatalogYear INNER JOIN LookupCatalogYear ON LookupCatalogYear.CatalogYearID = CourseCatalogYear.CatalogYearId AND LookupCatalogYear.CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) WHERE OpenStudies';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($courseId, $courseTitle);


    ?>

    <div class="header">

    </div>

    <?php

    echo '<table>
              <tr class="tableHeader">
                <td>Course Id</td>
                <td>Course Title</td>
              </tr>';

    if ($stmt->num_rows > 0) {


        while ($stmt->fetch()) {
            echo '<tr><td>'.$courseId.'</td><td>'.$courseTitle.'</td></tr>
';
        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="2">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('pageFoot.php');