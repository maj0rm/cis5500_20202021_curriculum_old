<?php

/**
 * CourseCreditValue.php
 *
 * Returns a list of courses and programs, as well as the minimum passing grade, program manager,
 * and consultant for each program.
 *
 * @author twhiten
 * @since 20201/03/03
 */

    include('pageHead.php');


    $query = 'SELECT CourseId, CourseTitle, ProgramId, ProgramTitle, ProgramMinimum, concat(pmTable.PersonFirstName , " " , pmTable.PersonLastName) AS "Program Manager", concat(cTable.PersonFirstName , " " , cTable.PersonLastName) AS "Consultant" FROM CourseCatalogYear INNER JOIN ProgramCourse ON ProgramCourse.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN ProgramCatalogYear ON ProgramCourse.ProgramCatalogYearId = ProgramCatalogYear.ProgramCatalogYearId INNER JOIN LookupProgramTitle ON ProgramCatalogYear.ProgramTitleId = LookupProgramTitle.ProgramTitleId INNER JOIN (SELECT PersonId, PersonFirstName, PersonLastName FROM Person) AS cTable ON CourseCatalogYear.ApprovedByConsultantPersonId = cTable.PersonId INNER JOIN (SELECT PersonId, PersonFirstName, PersonLastName FROM Person) AS pmTable ON pmTable.PersonId = CourseCatalogYear.ApprovedByPMPersonId';





    $stmt = $db->prepare($query);


    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($courseId, $courseTitle, $programId, $programTitle, $programMinimum, $programManager, $consultant);


    ?>

    <div class="header">

    </div>

    <?php

    echo '<table>';

    if ($stmt->num_rows > 0) {
        $currentCourse = null;


        while ($stmt->fetch()) {
            if ($currentCourse != $courseId) {
                echo '<tr class="tableHeader">
                        <td>'.$courseId.'</td>
                        <td colspan="2">'.$courseTitle.'</td>
                        <td colspan="2"/>
                      </tr>';
                $currentCourse = $courseId;
            }
            echo '<tr>
                    <td/>
                    <td>'.$programId.'</td>
                    <td colspan="2">'.$programTitle.'</td>
                    <td>'.$programMinimum.'</td>
                  </tr>
                  <tr>
                    <td colspan="3"/>
                    <td>Program Manager</td>
                    <td>'.$programManager.'</td>
                  </tr>
                  <tr>
                    <td colspan="3"/>
                    <td>Consultant</td>
                    <td>'.$consultant.'</td>
                  </tr>';

        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('pageFoot.php');