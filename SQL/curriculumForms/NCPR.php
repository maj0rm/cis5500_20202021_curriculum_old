<?php

/**
 * NCPR.php
 *
 * Returns a list of NCPR courses and the programs under those courses.
 *
 * @author twhiten
 * @since 20201/03/08
 */

    include('pageHead.php');


    $query = 'SELECT CourseId, CourseTitle, ProgramId, ProgramTitle, MinimumGrade, Hours, Credits  FROM CourseCatalogYear INNER JOIN LearningOutcome ON LearningOutcome.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN ProgramCourse ON ProgramCourse.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN ProgramCatalogYear ON ProgramCourse.ProgramCatalogYearId = ProgramCatalogYear.ProgramCatalogYearId INNER JOIN LookupProgramTitle ON ProgramCatalogYear.ProgramTitleId = LookupProgramTitle.ProgramTitleId WHERE CourseId LIKE "NCPR%" ORDER BY CourseId';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($CourseId, $CourseTitle, $ProgramId, $ProgramTitle, $Grade, $Hours, $Credits);


    ?>

    <div class="header">

    </div>

    <?php

    echo '<table>';

    if ($stmt->num_rows > 0) {

        $currentCourse = null;

        while ($stmt->fetch()) {

            if ($currentCourse != $CourseId) {
                echo '<tr class="tableHeader3">
                        <td>Course Code</td>
                        <td colspan="4">Course Title</td>
                        <td>Grade</td>
                        <td>Hours</td>
                        <td>Credits</td>
                      </tr>
                      <tr>
                        <td>'.$CourseId.'</td>
                        <td colspan="4">'.$CourseTitle.'</td>';

                if (empty($Grade)) {
                    echo '<td>P</td>';
                }
                else {
                    echo '<td>'.$Grade.'</td>';
                }

                  echo '<td>'.$Hours.'</td>
                        <td>'.$Credits.'</td>
                      </tr>
                      <tr>
                        <td class="tableHeader" colspan="7">Programs adopting this NCPR</td>
                        <td/>
                      </tr>';

                $currentCourse = $CourseId;
            }

            echo '<tr>
                    <td colspan="2"/>
                    <td>'.$ProgramId.'</td>
                    <td colspan="2">'.$ProgramTitle.'</td>
                    <td colspan="3"/>
                  </tr>';

        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td>'.$error.'</td></tr>';
    }

    echo '</table>';

    include('pageFoot.php');