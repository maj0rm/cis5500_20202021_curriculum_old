<?php

/**
 * AssessmentCategoryName.php
 *
 * Returns a list of assessment category names
 *
 * @author twhiten
 * @since 20201/03/03
 */

    include('pageHead.php');


    $query = 'SELECT AssessmentCategoryName FROM LookupAssessmentCategoryName';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($name);


    ?>

    <div class="header">

    </div>

    <?php

    echo '<table>';

    if ($stmt->num_rows > 0) {

        $currentCourse = null;

        while ($stmt->fetch()) {

            echo '<tr><td>'.$name.'</td></tr>';

        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td>'.$error.'</td></tr>';
    }

    echo '</table>';

    include('pageFoot.php');