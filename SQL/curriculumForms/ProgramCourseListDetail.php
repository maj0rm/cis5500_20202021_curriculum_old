<?php

/**
 * ProgramCourseListDetail.php
 *
 * Returns a list of courses and programs, with subtotals for credit courses, non-credit courses, and total values.
 *
 * @author twhiten
 * @since 20201/03/05
 */

include('pageHead.php');

$programSearch = null;
$yearSearch = null;


$query = 'SELECT CatalogYearName, ProgramId, ProgramTitle, courseId, CourseTitle, Elective, Hours, Credits, IF(GradeSchemeId=1,"P",MinimumGrade) AS RequiredGrade FROM CourseCatalogYear INNER JOIN ProgramCourse ON CourseCatalogYear.CourseCatalogYearId = ProgramCourse.CourseCatalogYearId INNER JOIN ProgramCatalogYear ON ProgramCourse.ProgramCatalogYearId = ProgramCatalogYear.ProgramCatalogYearId INNER JOIN LookupCatalogYear ON LookupCatalogYear.CatalogYearID = ProgramCatalogYear.CatalogYearId INNER JOIN LookupProgramTitle ON ProgramCatalogYear.ProgramTitleId = LookupProgramTitle.ProgramTitleId';

if (isset($_POST['program']) && !empty($_POST['program'])) {
    $programSearch = "%".$_POST['program']."%";
    $query .= " WHERE ProgramCatalogYear.ProgramId LIKE ?";
}
if (isset($_POST['year']) && !empty($_POST['year'])) {
    $yearSearch = $_POST['year'];
    $query .= " WHERE CatalogYearName LIKE ?";
}

$query .= " ORDER BY CatalogYearName, ProgramId, CourseId";

$stmt = $db->prepare($query);


if (isset($programSearch) && !isset($yearSearch)) {
    $searchTerm = $programSearch;
    $stmt->bind_param("s", $searchTerm);
}
if (!isset($programSearch) && isset($yearSearch)) {
    $searchTerm = $_POST['year'];
    $stmt->bind_param("s", $searchTerm);
}
if (isset($programSearch) && isset($yearSearch)) {
    $search1 = "%".$_POST['program']."%";
    $search2 = $_POST['year'];
    $stmt->bind_param("ss", $search1, $search2);
}

$stmt->execute();
$stmt->store_result();


$stmt->bind_result($catalogYearName, $programId, $programTitle, $courseId, $courseTitle, $elective, $hours, $credits, $requiredGrade);


?>



    <div class="header">
        <form action="ProgramCourseListDetail.php" method="post">
            <label for="program">Enter Program Name: </label>
            <input type="text" id="program" name="program"><br>
            <label for="year">Enter Year</label>
            <input type="text" id="year" name="year"><br><input type="submit" value="Search">
        </form>
    </div>

<?php

/*
 * New Program: ProgramID, ProgramName, CatalogYear
 * When a list of credit or non-credit course starts: Credit/Non-Credit, number of courses, headers
 * List of courses
 * List of credit/non-credit courses ends OR new program starts: Credit/Non-Credit, subtotals
 * List of program ends: Total
 */

if ($stmt->num_rows > 0) {

    $currentProgram = null;
    $currentYear = null;
    $numCourses = 0;
    $numCredits = 0;


    echo '<table>';

    while ($stmt->fetch()) {

        if ($currentYear != $catalogYearName) {
            echo '<tr><td>' . $catalogYearName . '</td><td colspan="7"/></tr>';

            $currentYear = $catalogYearName;
        }

        if ($currentProgram != $programId) {

            if (isset($currentProgram)) {
                echo '<tr class="tableHeader">
                            <td>Total Courses</td>
                            <td colspan="2">'.$numCourses.'</td>
                            <td colspan="2"/>
                            <td colspan="2">Total Credits</td>
                            <td>' . $numCredits . '</td>
                          </tr>';


                $numCourses = 0;
                $numCredits = 0;
            }


            echo '<tr>
                    <td>'.$programId.'</td>
                    <td colspan="2">'.$programTitle.'</td>
                    <td colspan="5"/>
                  </tr>
                  <tr class="tableHeader3">
                    <td/>
                    <td>Course Code</td>
                    <td colspan="2">Course Title</td>
                    <td>Minimum</td>
                    <td>Hours</td>
                    <td>Credits</td>
                    <td>Elective</td>
                  </tr>';

            $currentProgram = $programId;
        }

        echo '<tr>
                <td/>
                <td>'.$courseId.'</td>
                <td colspan="2">'.$courseTitle.'</td>
                <td>'.$requiredGrade.'</td>
                <td>'.$hours.'</td>
                <td>'.$credits.'</td>
                <td>';

        if ($elective == 1) {
            echo '<input type="checkbox" checked="checked" onclick="return false"/>';
        }
        else {
            echo '<input type="checkbox" onclick="return false"/>';
        }

        echo    '</td>
              </tr>';

        $numCourses++;
        $numCredits += $credits;
    }
}
else {
    $error = $db->errno . " " . $db->error;
    echo '<tr><td colspan="8">'.$error.'</td></tr>';
}

echo '</table>';

include('pageFoot.php');