<?php

/**
 * Entrepreneurship Extended.php
 *
 * Returns a list of courses and programs with a focus on entrepreneurial or business planning.
 *
 * @author twhiten
 * @since 20201/03/08
 */

    include('pageHead.php');



    $query = 'SELECT DISTINCT CourseId, CourseTitle, ProgramId, ProgramTitle, Hours 
FROM LookupCatalogYear INNER JOIN CourseCatalogYear ON LookupCatalogYear.CatalogYearID = CourseCatalogYear.CatalogYearId AND LookupCatalogYear.CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) INNER JOIN LearningOutcome ON LearningOutcome.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN ProgramCourse ON ProgramCourse.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN ProgramCatalogYear ON ProgramCourse.ProgramCatalogYearId = ProgramCatalogYear.ProgramCatalogYearId INNER JOIN LookupProgramTitle ON ProgramCatalogYear.ProgramTitleId = LookupProgramTitle.ProgramTitleId WHERE CourseTitle LIKE "%entrepreneur%" OR LearningOutcomeName LIKE "%entrepreneur%" OR CourseTitle LIKE "%Business Plan%" OR LearningOutcomeName LIKE "%Business Plan%" ORDER BY CourseId';


    $stmt = $db->prepare($query);


    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($courseId, $courseTitle, $programId, $programTitle, $hours);


    ?>

    <div class="header">
    </div>

    <?php

    echo '<table>
              ';

    if ($stmt->num_rows > 0) {
        $currentCourse = null;


        while ($stmt->fetch()) {
            if ($currentCourse != $courseId) {
                echo '<tr class="tableHeader">
                        <td>Course:</td>
                        <td>'.$courseId.'</td>
                        <td colspan="2">'.$courseTitle.'</td>
                        <td/>
                      </tr>
                      <tr class="tableHeader">
                        <td/>
                        <td colspan="3">Course Adopted In The Following Programs</td>
                        <td>Hours:</td>
                      </tr>';
                $currentCourse = $courseId;
            }
            echo '<tr>
                    <td></td>
                    <td>'.$programId.'</td>
                    <td colspan="2">'.$programTitle.'</td>
                    <td>'.$hours.'</td>
                  </tr>';
        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="5">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('pageFoot.php');