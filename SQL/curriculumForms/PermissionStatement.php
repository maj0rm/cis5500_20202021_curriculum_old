<?php

/**
 * PermissionStatement.php
 *
 * Return a list of courses that require permission statements
 *
 * @author twhiten
 * @since 20201/03/09
 */

    include('pageHead.php');


    $query = 'SELECT ParentCourseId, ChildCourseId, ChildTitle FROM CourseRequisite INNER JOIN LookupCatalogYear ON CourseRequisite.CatalogYearId = LookupCatalogYear.CatalogYearID AND LookupCatalogYear.CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) WHERE ChildCourseId = "Permission Statement" ORDER BY ParentCourseId';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($parentId, $childId, $childTitle);


    ?>

    <div class="header">

    </div>

    <?php



    if ($stmt->num_rows > 0) {

        $firstCourse = true;

        echo '<table>
              <tr class="tableHeader">
                <td>Prerequisite</td>
                <td colspan="2">Statement Wording</td>
                <td>Course Code</td>
              </tr>';


        while ($stmt->fetch()) {

            if ($firstCourse) {
                echo '<tr><td>'.$childId.'</td><td colspan="3"/></tr>';
                $firstCourse = false;
            }

            echo '<tr>
                    <td/>
                    <td colspan="2">'.$childTitle.'</td>
                    <td>'.$parentId.'</td>
                  </tr>
';
        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="2">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('pageFoot.php');