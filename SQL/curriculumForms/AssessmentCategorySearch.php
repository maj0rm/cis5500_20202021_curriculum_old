<?php

/**
 * AssessmentCategorySearch.php
 *
 * Returns a list of courses and programs with details on assessments involved with each program.
 * Allows users to search for specific assessments.
 *
 * @author twhiten
 * @since 20201/03/03
 */

    include('pageHead.php');


    $query = 'SELECT ProgramCatalogYear.ProgramId, ProgramTitle, CourseId, CourseTitle, AssessmentCategoryName FROM LookupCatalogYear INNER JOIN ProgramCatalogYear ON ProgramCatalogYear.CatalogYearId = LookupCatalogYear.CatalogYearID AND LookupCatalogYear.CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) INNER JOIN Program ON Program.ProgramId = ProgramCatalogYear.ProgramId INNER JOIN LookupProgramTitle ON ProgramCatalogYear.ProgramTitleId = LookupProgramTitle.ProgramTitleId INNER JOIN ProgramCourse ON ProgramCourse.ProgramCatalogYearId = ProgramCatalogYear.ProgramCatalogYearId INNER JOIN CourseCatalogYear ON CourseCatalogYear.CourseCatalogYearId = ProgramCourse.CourseCatalogYearId INNER JOIN AssessmentCategory ON AssessmentCategory.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN LookupAssessmentCategoryName ON LookupAssessmentCategoryName.AssessmentCategoryNameId = AssessmentCategory.AssessmentCategoryNameId';

    if (isset($_POST['search'])) {
        $query .= " WHERE AssessmentCategoryName LIKE ? ";
    };

    $query .= " ORDER BY AssessmentCategoryName, ProgramCatalogYear.ProgramId";

    $stmt = $db->prepare($query);


    if (isset($_POST['search'])) {
        $searchTerm = "%".$_POST['search']."%";
        $stmt->bind_param("s", $searchTerm);
    };

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($programId, $programTitle, $courseId,
        $courseTitle, $assessmentCategoryName);

    $resultArray = null;

    ?>

    <div class="header">
        <form action="AssessmentCategorySearch.php" method="post">
            <label for="search">Enter Assessment Name</label></br>
            <input type="text" id="search" name="search"><input type="submit" value="Search">
        </form>
    </div>

    <?php

    echo '<table>
              <tr class="tableHeader">
                <td>Assessment Category</td><td colspan="3"/>
              </tr>
              <tr>
                <td/><td>Program Code</td><td>Program Title</td><td/>
              </tr>
              <tr>
                <td colspan="2"/><td>Course Code</td><td>Course Title</td>
              </tr>';

    if ($stmt->num_rows > 0) {
        $currentCategory = null;
        $currentProgram = null;


        while ($stmt->fetch()) {
            if ($currentCategory != $assessmentCategoryName) {
                echo '<tr class="tableHeader2"><td>'.$assessmentCategoryName.'</td><td colspan="3"></td></tr>';
                $currentCategory = $assessmentCategoryName;
            }
            if ($currentProgram != $programId) {
                echo '<tr><td/><td>'.$programId.'</td><td>'.$programTitle.'</td><td/></tr>';
                $currentProgram = $programId;
            }
            echo '<tr><td colspan="2"></td><td>'.$courseId.'</td><td>'.$courseTitle.'</td></tr>';
        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="4">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('pageFoot.php');