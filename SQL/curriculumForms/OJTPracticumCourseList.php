<?php

/**
 * OJTPracticumCourseList.php
 *
 * Returns a detailed list of OJT courses.
 *
 * @author twhiten
 * @since 20201/03/09
 */

    include('pageHead.php');


    $query = 'SELECT CourseId, CourseTitle, Hours, Credits, GradeSchemeName FROM LookupCatalogYear INNER JOIN CourseCatalogYear ON LookupCatalogYear.CatalogYearID = CourseCatalogYear.CatalogYearId AND LookupCatalogYear.CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) INNER JOIN LookupGradeScheme ON CourseCatalogYear.GradeSchemeId = LookupGradeScheme.GradeSchemeId WHERE CourseTitle LIKE "%OJT%" OR CourseTitle LIKE "%Practicum%" OR CourseTitle LIKE "%Job%" OR CourseTitle LIKE "%Clinical%" OR CourseTitle LIKE "%Capstone%"';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($courseId, $courseTitle, $hours, $credits, $gradeScheme);


    ?>

    <div class="header">

    </div>

    <?php

    echo '<table>';

    if ($stmt->num_rows > 0) {


        echo '<tr class="tableHeader3">
                <td>Course Code</td>
                <td colspan="2">Course Title</td>
                <td>Grade Scheme</td>
                <td>Hours</td>
                <td>Credits</td>
              </tr>';

        while ($stmt->fetch()) {

                echo '<tr>
                        <td>'.$courseId.'</td>
                        <td colspan="2">'.$courseTitle.'</td>
                        <td>'.$gradeScheme.'</td>
                        <td>'.$hours.'</td>
                        <td>'.$credits.'</td>
                      </tr>';


        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td>'.$error.'</td></tr>';
    }

    echo '</table>';

    include('pageFoot.php');