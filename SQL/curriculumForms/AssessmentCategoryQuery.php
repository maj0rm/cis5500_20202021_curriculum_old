<?php

/**
 * AssessmentCategoryQuery.php
 *
 * Returns a list of courses and programs with details on assessments involved with each program.
 * Allows users to search for specific assessments.
 *
 * @author twhiten
 * @since 20201/03/03
 */

    include('pageHead.php');



    $query = 'SELECT CatalogYearName, ProgramCatalogYear.ProgramId, ProgramTitle, CourseId, CourseTitle, AssessmentCategoryName FROM LookupCatalogYear INNER JOIN ProgramCatalogYear ON ProgramCatalogYear.CatalogYearId = LookupCatalogYear.CatalogYearID AND LookupCatalogYear.CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) INNER JOIN Program ON Program.ProgramId = ProgramCatalogYear.ProgramId INNER JOIN LookupProgramTitle ON ProgramCatalogYear.ProgramTitleId = LookupProgramTitle.ProgramTitleId INNER JOIN ProgramCourse ON ProgramCourse.ProgramCatalogYearId = ProgramCatalogYear.ProgramCatalogYearId INNER JOIN CourseCatalogYear ON CourseCatalogYear.CourseCatalogYearId = ProgramCourse.CourseCatalogYearId INNER JOIN AssessmentCategory ON AssessmentCategory.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN LookupAssessmentCategoryName ON LookupAssessmentCategoryName.AssessmentCategoryNameId = AssessmentCategory.AssessmentCategoryNameId';

    if (isset($_POST['search'])) {
        $query .= " WHERE AssessmentCategoryName LIKE ? ";
    };

    $query .= " ORDER BY AssessmentCategoryName";

    $stmt = $db->prepare($query);


    if (isset($_POST['search'])) {
        $searchTerm = "%".$_POST['search']."%";
        $stmt->bind_param("s", $searchTerm);
    };

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($catalogYearName, $programId, $programTitle, $courseId,
        $courseTitle, $assessmentCategoryName);


    ?>

    <div class="header">
        <form action="AssessmentCategoryQuery.php" method="post">
            <label for="search">Enter Assessment Name</label></br>
            <input type="text" id="search" name="search"><input type="submit" value="Search">
        </form>
    </div>

    <?php

    echo '<table>
              <tr class="tableHeader">
                <td>Year</td>
                <td>Assessment Category</td>
                <td>Program ID</td>
                <td>Program Title</td>
                <td>Course Catalog Year</td>
                <td>Course Title</td>
              </tr>';

    if ($stmt->num_rows > 0) {
        $currentYear= 0;
        $currentCategory = null;


        while ($stmt->fetch()) {
            if ($currentYear != $catalogYearName) {
                echo '<tr class="tableHeader"><td>'.$catalogYearName.'</td><td colspan="5"></td></tr>';
                $currentYear = $catalogYearName;
            }
            if ($currentCategory != $assessmentCategoryName) {
                echo '<tr class="tableHeader"><td></td><td>'.$assessmentCategoryName.'</td><td colspan="4"></td></tr>';
                $currentCategory = $assessmentCategoryName;
            }
            echo '<tr><td colspan="2"></td><td>'.$programId.'</td><td>'.$programTitle.'</td><td>'.$catalogYearName.'</td>
                      <td>'.$courseTitle.'</td></tr>';
        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="6">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('pageFoot.php');