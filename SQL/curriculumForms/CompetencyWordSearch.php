<?php

    /**
     * CourseCreditValue.php
     *
     * Returns a list of courses with learning outcomes and competencies involved with that course.
     *
     * @author twhiten
     * @since 20201/03/04
     */

    include('pageHead.php');


    $query = 'SELECT CourseId, CourseTitle, LearningOutcomeName, LearningCompetencyName FROM LookupCatalogYear INNER JOIN CourseCatalogYear ON LookupCatalogYear.CatalogYearID = CourseCatalogYear.CatalogYearId AND LookupCatalogYear.CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) INNER JOIN LearningOutcome ON LearningOutcome.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN LearningCompetency ON LearningOutcome.LearningOutcomeId = LearningCompetency.LearningOutcomeId';


    if (isset($_POST['search'])) {
        $query .= "WHERE LearningCompetencyName LIKE ?";
    }

    $query .= " ORDER BY CourseId, LearningOutcomeName";

    $stmt = $db->prepare($query);


    if (isset($_POST['search'])) {
        $searchTerm = "%".$_POST['search']."%";
        $stmt->bind_param("s", $searchTerm);
    };

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($courseId, $courseTitle, $oName, $cName);


    ?>

    <div class="header">
        <form action="CompetencyWordSearch.php" method="post">
            <label for="search">Search Outcomes/Competencies</label></br>
            <input type="text" id="search" name="search"><input type="submit" value="Search">
        </form>
    </div>

    <?php

    echo '<table>';

    if ($stmt->num_rows > 0) {
        $currentCourse = null;
        $currentOutcome = null;
        $outId = 0;
        $compId = 0;


        while ($stmt->fetch()) {
            if ($currentCourse != $courseId) {
                echo '<tr class="tableHeader2"><td>'.$courseId.'</td><td>'.$courseTitle.'</td><td colspan="3"></td></tr>';
                $currentCourse = $courseId;
                $outId = 0;
                $compId = 0;
            }
            if ($currentOutcome != $oName) {
                $outId += 1;
                echo '<tr><td/><td colspan="4">'.$oName.'</td><td/></tr>';
                $currentOutcome = $oName;
            }
            $compId += 1;
            echo '<tr><td colspan="2"/><td colspan="3">'.$cName.'</td></tr>';
        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="4">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('pageFoot.php');