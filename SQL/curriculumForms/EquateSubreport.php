<?php

/**
 * Equate Subreport.php
 *
 * Return a list of courses have have the "equate" requisite type
 *
 * @author twhiten
 * @since 20201/03/05
 */

    $subdb = new mysqli('localhost', 'root', '', 'twhiten_curriculum');


    $subquery = 'SELECT ChildCourseId, ChildTitle FROM LookupRequisiteType INNER JOIN CourseRequisite ON CourseRequisite.RequisiteTypeId = LookupRequisiteType.RequisiteId INNER JOIN LookupCatalogYear ON CourseRequisite.CatalogYearId = LookupCatalogYear.CatalogYearId WHERE RequisiteName LIKE "Equate"';

    if (isset($courseSearch)) {
        $subquery .= ' AND ParentCourseId = ?';
    }
    if (isset($yearSearch)) {
        $subquery .= ' AND CatalogYearName = ?';
    }
    else {
        $subquery .= ' AND CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear)';
    }


    $substmt = $subdb->prepare($subquery);

    if (!isset($yearSearch)) {
        $substmt->bind_param('s', $courseSearch);
    }
    else {
        $substmt->bind_param('ss', $courseSearch, $yearSearch);
    }

    $substmt->execute();
    $substmt->store_result();


    $substmt->bind_result($subId, $subTitle);




    if ($substmt->num_rows > 0) {

        while ($substmt->fetch()) {
            echo '<p>'.$subId.': '.$subTitle.'</p>';
        }

    }
    else {
        echo '<p>None</p>';
    }

    $substmt->close();
    $subdb->close();