<?php

/**
 * QF132CourseOutline.php
 *
 * Returns a list of assessment category names
 *
 * @author twhiten
 * @since 20201/03/03
 *
 * @todo Add subreports
 */

    include('pageHead.php');


    $courseSearch = null;
    $yearSearch = null;
    $query = 'SELECT CatalogYearName, CourseTitle, CourseId, CourseDescription, GradeSchemeName, MinimumGrade, OutcomeHours, Credits, Hours, Research, NewCourse, ReplacingExisting, OriginalCatYear, CurrentVersionCat, RevisionLevel, CourseVersion, RevisionDate, AuthorizedByPersonId, SupportingDoc, AdditionalInfo, SubjectMatterExpert, pmTable.personName, ApprovedDate, cTable.personName, ApprovedDateCC, InstructionalMethodId, AssessmentNote FROM LookupCatalogYear INNER JOIN CourseCatalogYear ON LookupCatalogYear.CatalogYearID = CourseCatalogYear.CatalogYearId INNER JOIN LookupGradeScheme ON CourseCatalogYear.GradeSchemeId = LookupGradeScheme.GradeSchemeId INNER JOIN (SELECT personId, concat(PersonFirstName, " ", personLastName) AS personName FROM Person) AS pmTable ON pmTable.PersonId = CourseCatalogYear.ApprovedByPMPersonId INNER JOIN (SELECT personId, concat(PersonFirstName, " ", personLastName) AS personName FROM Person) AS cTable ON cTable.PersonId = CourseCatalogYear.ApprovedByConsultantPersonId WHERE CourseId = ?';

    if (isset($_POST['course'])) {
        $courseSearch = $_POST['course'];
    }

    if (isset($_POST['year']) && !empty($_POST['year'])) {
        $yearSearch = $_POST['year'];
        $query .= " AND CatalogYearName = ?";
    }
    else {
        $query .= " AND CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear)";
    }

    $stmt = $db->prepare($query);




    if (isset($yearSearch)) {
        $stmt->bind_param("ss", $courseSearch, $yearSearch);
    }
    else {
        $stmt->bind_param("s", $courseSearch);
    }



    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($catalogYear, $courseTitle, $courseId, $courseDescription, $gradeScheme, $minimumGrade, $outcomeHours, $credits, $hours, $research, $newCourse, $replacingExisting, $originalCatYear, $currentVersionCat, $revisionLevel, $courseVersion, $revisionDate, $authorizedBy, $supportingDoc, $additionalInfo, $subjectMatterExpert, $approvedPM, $approvedDate, $approvedConsultant, $approvedCC, $instructionalMethod, $assessmentNote);


    ?>

    <div class="header">
        <table>
            <tr>
                <td>Quality Form 132</td>
                <td>Related Procedure A01</td>
                <td>Revision: TWO</td>
                <td>Issue Date:            </td>
            </tr>
        </table>
        <div class="header">
            <form action="QF132CourseOutline.php" method="post">
                <label for="course">Enter Course ID: </label>
                <input type="text" id="course" name="course"><br>
                <label for="year">Enter Year</label>
                <input type="text" id="year" name="year"><br><input type="submit" value="Search">
            </form>
        </div>
    </div>

    <?php


    if ($stmt->num_rows == 1) {
        $stmt->fetch();


            echo '<div>
                    <h1>Section 1:</h1>
                    <p>Course Title: '.$courseTitle.'</p>
                    <p>Course Code: '.$courseId.'</p>
                    <p>Course Description: '.$courseDescription.'</p>
                    <p>Grade Scheme: '.$gradeScheme.'   Minimum Grade: '.$minimumGrade.'  Instructional Method: '.$instructionalMethod.'</p>
                    <p>Course Value: Outcome Hours: '.$outcomeHours.' OR Credits: '.$credits.' Hour: '.$hours.'</p>
                    <p>Prerequisites:</p>';
                    include ("PrerequisiteSubreport.php");
            echo '<p>Co-requisites:</p>';
                    include ("CorequisiteSubreport.php");


            echo '</div>
                  <div>
                    <h1>Section 2:</h1>
                    <h2>Learning Outcomes and Competencies</h2>';
                include("LearningOutcomeSubreport.php");

            echo '</div>
                  <div>
                    <h1>Section 3:</h1>
                    <p>Assessment Categories:</p>';
                    include ("AssessmentCategorySubreport.php");

            echo   '<p>Assessment Note: '.$assessmentNote.'</p>
                    <p>Research Component: ';

                    if ($research == 1) {
                        echo '<input type="checkbox" checked="checked" onclick="return false"/>';
                    }
                    else {
                        echo '<input type="checkbox" onclick="return false"/>';
                    }

            echo                                            '</p>
                  </div>
                  <div>
                    <h1>Section 4:</h1>
                    <p>(Administrator Use Only)</p>
                    <p>New course? ';

        if ($newCourse == 1) {
            echo '<input type="checkbox" checked="checked" onclick="return false"/>';
        }
        else {
            echo '<input type="checkbox" onclick="return false"/>';
        }

        echo                                            '</p>
                    <p>Replacing existing course? ';

        if ($research == 1) {
            echo '<input type="checkbox" checked="checked" onclick="return false"/>';
        }
        else {
            echo '<input type="checkbox" onclick="return false"/>';
        }

        echo                                            '</p>
                  <h4>Record name/title of old course (if applicable)</h4>';
                    include("CourseReplacementSubreport.php");
        echo     '<h4>Course Equivalents</h4>';
                    include("EquateSubreport.php");
        echo     '<p>See Quality Procedure 01 for more details</p>
                  <p>Catalog Year of original course implementation: '.$originalCatYear.'</p>
                  <p>Catalog Year of current version implementation: '.$currentVersionCat.'</p>
                  <p>Revision Level: '.$revisionLevel.'   Version: '.$courseVersion.'  Date: '.$revisionDate.' Authorized By: '.$authorizedBy.'</p>
                  <p>Accreditation or Supporting Documents</p>
                  <p>'.$supportingDoc.'</p>
                  <p>Additional Information:</p>
                  <p>'.$additionalInfo.'</p>
                  <p>Approved By Program Manager:</p>
                  <p>'.$approvedPM.'  (Date Approved: '.$approvedDate.')</p>
                  <p>Approved By Consultant:</p>
                  <p>'.$approvedConsultant.'  (Date Approved: '.$approvedCC.')</p>
                </div>';
    }
    else if ($stmt->num_rows == 0) {
        echo '<h3 class="header">No course found</h3>';
    }
    else if ($stmt->num_rows < 0) {
        $error = $db->errno . " " . $db->error;
        echo '<p>'.$error.'</p>';
    }


    include('pageFoot.php');