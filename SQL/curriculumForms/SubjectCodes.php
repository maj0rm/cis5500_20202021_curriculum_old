<?php

/**
 *
 * ReserveSubjectCode.php
 *
 * Returns reserved subject codes
 *
 * @author twhiten
 * @since 20201/03/09
 */

    include('pageHead.php');


    $query = 'SELECT FourLetterCode, subjectDescription FROM subjectCodes';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($code, $description);


    ?>

    <div class="header">

    </div>

    <?php

    echo '<table>';

    if ($stmt->num_rows > 0) {


        while ($stmt->fetch()) {

            echo '<tr>
                    <td>'.$code.'</td>
                    <td>'.$description.'</td>
                  </tr>';

        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td>'.$error.'</td></tr>';
    }

    echo '</table>';

    include('pageFoot.php');