<?php

/**
 * CourseHourBreakdown.php
 *
 * Returns a list of courses and programs, with subtotals for credit courses, non-credit courses, and total values.
 *
 * @author twhiten
 * @since 20201/03/05
 */

include('pageHead.php');

$_POST['program'] = "ACC";

$programSearch = null;
$yearSearch = null;


$query = 'SELECT CatalogYearName, ProgramCatalogYear.ProgramId, LookupProgramTitle.ProgramTitle, CourseCatalogYear.CourseId, CourseCatalogYear.CourseTitle, CourseCatalogYear.Hours, CourseCatalogYear.Credits, CourseCatalogYear.ClassHours, CourseCatalogYear.LabHours, CourseCatalogYear.ClinicalHours, CourseCatalogYear.OJTPracticumHours, CourseCatalogYear.InternshipHours, CourseCatalogYear.InstructionalMethodId, If(CourseCatalogYear.Credits>0,"Credit Course","Non-Credit Course") AS CreditCourse, creditCount.rowCount, nonCreditCount.rowCount FROM LookupProgramTitle INNER JOIN ProgramCatalogYear ON LookupProgramTitle.ProgramTitleId = ProgramCatalogYear.ProgramTitleId INNER JOIN ProgramCourse ON ProgramCatalogYear.ProgramCatalogYearId = ProgramCourse.ProgramCatalogYearId INNER JOIN CourseCatalogYear ON ProgramCourse.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN LookupCatalogYear ON ProgramCatalogYear.CatalogYearID = LookupCatalogYear.CatalogYearID INNER JOIN (SELECT ProgramId, count(CourseId) AS rowCount FROM CourseCatalogYear INNER JOIN ProgramCourse ON ProgramCourse.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN ProgramCatalogYear ON ProgramCatalogYear.ProgramCatalogYearId = ProgramCourse.ProgramCatalogYearId WHERE Credits > 0 GROUP BY ProgramId) AS creditCount ON creditCount.ProgramId = ProgramCatalogYear.ProgramId INNER JOIN (SELECT ProgramId, count(CourseId) AS rowCount FROM CourseCatalogYear INNER JOIN ProgramCourse ON ProgramCourse.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN ProgramCatalogYear ON ProgramCatalogYear.ProgramCatalogYearId = ProgramCourse.ProgramCatalogYearId WHERE Credits <= 0 GROUP BY ProgramId) AS nonCreditCount ON nonCreditCount.ProgramId = ProgramCatalogYear.ProgramId';

if (isset($_POST['program']) && strlen($_POST['program']) > 0) {
    $programSearch = "%".$_POST['program']."%";
    $query .= " WHERE ProgramCatalogYear.ProgramId LIKE ?";
}
if (isset($_POST['year']) && strlen($_POST['year']) > 0) {
    $yearSearch = $_POST['year'];
    $query .= " WHERE CatalogYearName LIKE ?";
}

$query .= " ORDER BY ProgramId, CreditCourse, CourseId, CatalogYearName DESC";

$stmt = $db->prepare($query);


if (isset($programSearch) && !isset($yearSearch)) {
    $searchTerm = $programSearch;
    $stmt->bind_param("s", $searchTerm);
}
if (!isset($programSearch) && isset($yearSearch)) {
    $searchTerm = $_POST['year'];
    $stmt->bind_param("s", $searchTerm);
}
if (isset($programSearch) && isset($yearSearch)) {
    $search1 = "%".$_POST['program']."%";
    $search2 = $_POST['year'];
    $stmt->bind_param("ss", $search1, $search2);
}

$stmt->execute();
$stmt->store_result();


$stmt->bind_result($CatalogYearName, $ProgramId, $ProgramTitle, $CourseId, $CourseTitle, $Hours, $Credits, $ClassHours, $LabHours, $ClinicalHours, $OJTHours, $InternHours, $InstructionalId, $isCredit, $creditRows, $nonCreditRows);


?>



    <div class="header">
        <form action="CourseHourBreakdown.php" method="post">
            <label for="program">Enter Program Name: </label>
            <input type="text" id="program" name="program"><br>
            <label for="year">Enter Year</label>
            <input type="text" id="year" name="year"><br><input type="submit" value="Search">
        </form>
    </div>

<?php

/*
 * New Program: ProgramID, ProgramName, CatalogYear
 * When a list of credit or non-credit course starts: Credit/Non-Credit, number of courses, headers
 * List of courses
 * List of credit/non-credit courses ends OR new program starts: Credit/Non-Credit, subtotals
 * List of program ends: Total
 */

if ($stmt->num_rows > 0) {
    $currentProgram = null;
    $currentCredit = null;

    $hourSubtotal = 0;
    $hourTotal = 0;
    $subtotalArray = array('Class'=>0, 'Lab'=>0, 'Clinical'=>0, 'OJT'=>0, 'Internship'=>0);
    $totalArray = array('Class'=>0, 'Lab'=>0, 'Clinical'=>0, 'OJT'=>0, 'Internship'=>0);
    $creditSubtotal = 0;
    $creditTotal = 0;

    echo '<table>';

    while ($stmt->fetch()) {
        if ($currentProgram != $ProgramId) {
            if (isset($currentProgram)) {

                echo '<tr>
                        <td/>
                        <td>'.$currentCredit.'</td>
                        <td>Subtotal:</td>
                        <td>'.$creditSubtotal.'</td>
                        <td>'.$hourSubtotal.'</td>
                        <td>'.$subtotalArray['Class'].'</td>
                        <td>'.$subtotalArray['Lab'].'</td>
                        <td>'.$subtotalArray['Clinical'].'</td>
                        <td>'.$subtotalArray['OJT'].'</td>
                        <td>'.$subtotalArray['Internship'].'</td>
                      </tr>';

                $hourTotal += $hourSubtotal;
                $creditTotal += $creditSubtotal;
                $totalArray['Class'] += $subtotalArray['Class'];
                $totalArray['Lab'] += $subtotalArray['Lab'];
                $totalArray['Clinical'] += $subtotalArray['Clinical'];
                $totalArray['OJT'] += $subtotalArray['OJT'];
                $totalArray['Internship'] += $subtotalArray['Internship'];

                $hourSubtotal = 0;
                $creditSubtotal = 0;

                $subtotalArray = array('Class'=>0, 'Lab'=>0, 'Clinical'=>0, 'OJT'=>0, 'Internship'=>0);

                echo '<tr>
                              <td/>
                              <td>'.$currentProgram.'</td>
                              <td>Total Hours:</td>
                              <td>'.$creditTotal.'</td>
                              <td>'.$hourTotal.'</td>
                              <td>'.$totalArray['Class'].'</td>
                              <td>'.$totalArray['Lab'].'</td>
                              <td>'.$totalArray['Clinical'].'</td>
                              <td>'.$totalArray['OJT'].'</td>
                              <td>'.$totalArray['Internship'].'</td>
                          </tr>';

                $hourTotal = 0;
                $creditTotal = 0;

                $totalArray = array('Class'=>0, 'Lab'=>0, 'Clinical'=>0, 'OJT'=>0, 'Internship'=>0);
            }
            echo '<tr>
                        <td>'.$ProgramId.'</td>
                        <td colspan="7">'.$ProgramTitle.'</td>
                        <td>Catalog Year</td>
                        <td>'.$CatalogYearName.'</td>
                      </tr>
                      <tr>
                        <td>'.$isCredit.'</td>
                        <td>Count:</td>';

            if ($isCredit == "Credit Course") {
                echo '<td>'.$creditRows.'</td>';
            }
            else {
                echo '<td>'.$nonCreditRows.'</td>';
            }

            echo  '<td>Credits</td>
                        <td>Total Hours</td>
                        <td>Class</td>
                        <td>Lab</td>
                        <td>Clinical</td>
                        <td>Practicum</td>
                        <td>Internship</td>
                      </tr>';

            $currentProgram = $ProgramId;
            $currentCredit = $isCredit;
        }
        if ($currentCredit != $isCredit) {
            echo '<tr>
                        <td/>
                        <td>'.$currentCredit.'</td>
                        <td>Subtotal:</td>
                        <td>'.$creditSubtotal.'</td>
                        <td>'.$hourSubtotal.'</td>
                        <td>'.$subtotalArray['Class'].'</td>
                        <td>'.$subtotalArray['Lab'].'</td>
                        <td>'.$subtotalArray['Clinical'].'</td>
                        <td>'.$subtotalArray['OJT'].'</td>
                        <td>'.$subtotalArray['Internship'].'</td>
                      </tr>
                      <tr>
                        <td>'.$isCredit.'</td>
                        <td>Count:</td>';
            if ($isCredit == "Credit Course") {
                echo '<td>'.$creditRows.'</td>';
            }
            else {
                echo '<td>'.$nonCreditRows.'</td>';
            }
            echo '<td>Credits</td>
                        <td>Total Hours</td>
                        <td>Class</td>
                        <td>Lab</td>
                        <td>Clinical</td>
                        <td>Practicum</td>
                        <td>Internship</td>
                      </tr>';

            $hourTotal += $hourSubtotal;
            $creditTotal += $creditSubtotal;
            $totalArray['Class'] += $subtotalArray['Class'];
            $totalArray['Lab'] += $subtotalArray['Lab'];
            $totalArray['Clinical'] += $subtotalArray['Clinical'];
            $totalArray['OJT'] += $subtotalArray['OJT'];
            $totalArray['Internship'] += $subtotalArray['Internship'];
            $currentCredit = $isCredit;

            $hourSubtotal = 0;
            $creditSubtotal = 0;

            $subtotalArray = array('Class'=>0, 'Lab'=>0, 'Clinical'=>0, 'OJT'=>0, 'Internship'=>0);
        }
        echo '<tr>
                    <td>'.$CourseId.'</td>
                    <td colspan="2">'.$CourseTitle.'</td>
                    <td>'.$Credits.'</td>
                    <td>'.$Hours.'</td>
                    <td>'.$ClassHours.'</td>
                    <td>'.$LabHours.'</td>
                    <td>'.$ClinicalHours.'</td>
                    <td>'.$OJTHours.'</td>
                    <td>'.$InternHours.'</td>
                  </tr>';


        $subtotalArray['Class'] += $ClassHours;
        $subtotalArray['Lab'] += $LabHours;
        $subtotalArray['Clinical'] += $ClinicalHours;
        $subtotalArray['OJT'] += $OJTHours;
        $subtotalArray['Internship'] += $InternHours;
        $creditSubtotal += $Credits;
        $hourSubtotal += $Hours;
    }

    echo '<tr>
                <td/>
                <td>'.$currentCredit.'</td>
                <td>Subtotal:</td>
                <td>'.$creditSubtotal.'</td>
                <td>'.$hourSubtotal.'</td>
                <td>'.$subtotalArray['Class'].'</td>
                <td>'.$subtotalArray['Lab'].'</td>
                <td>'.$subtotalArray['Clinical'].'</td>
                <td>'.$subtotalArray['OJT'].'</td>
                <td>'.$subtotalArray['Internship'].'</td>
              </tr>';

    $hourTotal += $hourSubtotal;
    $creditTotal += $creditSubtotal;
    $totalArray['Class'] += $subtotalArray['Class'];
    $totalArray['Lab'] += $subtotalArray['Lab'];
    $totalArray['Clinical'] += $subtotalArray['Clinical'];
    $totalArray['OJT'] += $subtotalArray['OJT'];
    $totalArray['Internship'] += $subtotalArray['Internship'];

    echo '<tr>
                  <td/>
                  <td>'.$currentProgram.'</td>
                  <td>Total Hours:</td>
                  <td>'.$creditTotal.'</td>
                  <td>'.$hourTotal.'</td>
                  <td>'.$totalArray['Class'].'</td>
                  <td>'.$totalArray['Lab'].'</td>
                  <td>'.$totalArray['Clinical'].'</td>
                  <td>'.$totalArray['OJT'].'</td>
                  <td>'.$totalArray['Internship'].'</td>
              </tr>';
}
else {
    $error = $db->errno . " " . $db->error;
    echo '<tr><td colspan="6">'.$error.'</td></tr>';
}

echo '</table>';

include('pageFoot.php');