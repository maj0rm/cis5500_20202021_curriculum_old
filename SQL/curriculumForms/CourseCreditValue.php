<?php

/**
 * CourseCreditValue.php
 *
 * Returns a list of courses and programs sorted by Credit value, then CourseId, while allowing a user
 * to select a specific credit value.
 *
 * @author twhiten
 * @since 20201/03/04
 * @todo Currently the null and 0 credit values are considered similar, need to find a way to differentiate that
 */

    include('pageHead.php');


    $query = 'SELECT CatalogYearName, CourseId, CourseTitle, Credits, ProgramId, ProgramTitle, Hours FROM LookupCatalogYear INNER JOIN CourseCatalogYear ON LookupCatalogYear.CatalogYearID = CourseCatalogYear.CatalogYearId AND LookupCatalogYear.CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) INNER JOIN ProgramCourse ON ProgramCourse.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN ProgramCatalogYear ON ProgramCatalogYear.ProgramCatalogYearId = ProgramCourse.ProgramCatalogYearId INNER JOIN LookupProgramTitle ON ProgramCatalogYear.ProgramTitleId = LookupProgramTitle.ProgramTitleId';

    if (isset($_POST['search'])) {
        $query .= " WHERE Credits = ?";
    }

    $query .= " ORDER BY Credits, CourseId";

    $stmt = $db->prepare($query);



    if (isset($_POST['search'])) {
        $searchTerm = $_POST['search'];
        $stmt->bind_param("s", $searchTerm);
    };



    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($catalogYearName, $courseId, $courseTitle, $credits, $programId, $programTitle, $hours);


    ?>

    <div class="header">
        <form action="CourseCreditValue.php" method="post">
            <label for="search">Enter Credit Value</label></br>
            <input type="text" id="search" name="search"><input type="submit" value="Search">
        </form>
    </div>

    <?php

    echo '<table>';

    if ($stmt->num_rows > 0) {
        $currentCredit = 1;
        $currentCourse = null;


        while ($stmt->fetch()) {
            if ($currentCredit != $credits) {
                echo '<tr class="tableHeader">
                        <td>Credit Value</td>';
                        if (!isset($hours)) {
                           echo '<td>0</td>';
                       }
                       else {
                           echo '<td>'.$credits.'</td>';
                       }
                 echo '<td colspan="3"/>
                      </tr>';

                $currentCredit = $credits;
            }
            if ($currentCourse != $courseId) {
                echo '<tr>
                        <td/>
                        <td>'.$courseId.'</td>
                        <td colspan="2">'.$courseTitle.'</td>';
                       if (!isset($hours)) {
                           echo '<td>0 hrs</td>';
                       }
                       else {
                           echo '<td>'.$hours.' hrs</td>';
                       }
                 echo '</tr>';
                $currentCourse = $courseId;
            }
            echo '<tr>
                    <td colspan="2"/>
                    <td>'.$programId.'</td>
                    <td>'.$programTitle.'</td>
                    <td/>
                  </tr>';

        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('pageFoot.php');