<?php

/**
 * Prerequisite Subreport.php
 *
 * Return a list of courses that are prerequisites for other courses
 *
 * @author twhiten
 * @since 20201/03/08
 */





    $subdb = new mysqli('localhost', 'root', '', 'twhiten_curriculum');

    $subquery = 'SELECT ChildCourseId, ChildTitle FROM LookupRequisiteType INNER JOIN CourseRequisite ON CourseRequisite.RequisiteTypeId = LookupRequisiteType.RequisiteId INNER JOIN LookupCatalogYear ON CourseRequisite.CatalogYearId = LookupCatalogYear.CatalogYearId';



    $subquery .= " WHERE RequisiteName = 'Prerequisite'";

    if (isset($courseSearch)) {
        $subquery .= ' AND ParentCourseId = ?';
    }
    if (isset($yearSearch)) {
        $subquery .= ' AND CatalogYearName = ?';
    }
    else {
        $subquery .= ' AND CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear)';
    }

    $subquery .= " AND ChildTitle IS NOT NULL";

    $substmt = $subdb->prepare($subquery);

    if (!isset($yearSearch)) {
        $substmt->bind_param('s', $courseSearch);
    }
    else {
        $substmt->bind_param('ss', $courseSearch, $yearSearch);
    }

    $substmt->execute();
    $substmt->store_result();


    $substmt->bind_result($subId, $subTitle);



    if ($substmt->num_rows > 0) {
        echo '<table>
              <tr class="tableHeader">
                <td>Course Id</td>
                <td>Course Title</td>
              </tr>';

        while ($substmt->fetch()) {
            echo '<tr><td>'.$subId.'</td><td>'.$subTitle.'</td></tr>
';
        }

        echo '</table>';
    }
    else {
        echo '<p>None</p>';
    }

    $substmt->close();
    $subdb->close();

