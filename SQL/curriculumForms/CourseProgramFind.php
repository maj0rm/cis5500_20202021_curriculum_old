<?php

/**
 * CourseProgramFind.php
 *
 * Connect to a database and return a list of courses with related program information
 *
 * @author twhiten
 * @since 20201/03/05
 */

    include('pageHead.php');


    $query = 'SELECT CourseId, CourseTitle, ProgramId, ProgramTitleId, ProgramMinimum, personFirstName, personLastName, Hours FROM LookupCatalogYear INNER JOIN CourseCatalogYear ON CourseCatalogYear.CatalogYearId = LookupCatalogYear.CatalogYearID AND LookupCatalogYear.CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) INNER JOIN ProgramCourse ON CourseCatalogYear.CourseCatalogYearId = ProgramCourse.CourseCatalogYearId INNER JOIN ProgramCatalogYear ON ProgramCourse.ProgramCatalogYearId = ProgramCatalogYear.ProgramCatalogYearId INNER JOIN Person ON ProgramCatalogYear.ProgramManagerPersonId = Person.PersonId';

    if (isset($_POST['search'])) {
        $query .= " WHERE CourseTitle LIKE ?";
    };

    $query .= " ORDER BY CourseId, ProgramId";


    $stmt = $db->prepare($query);


    if (isset($_POST['search'])) {
        $searchTerm = "%".$_POST['search']."%";
        $stmt->bind_param("s", $searchTerm);
    }

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($courseId, $courseTitle, $programId, $programTitle, $programMinimum, $pmFirstName, $pmLastName, $hours);


    ?>

    <div class="header">
        <form action="CourseProgramFind.php" method="post">
            <label for="search">Course Title Search</label></br>
            <input type="text" id="search" name="search"><input type="submit" value="Search">
        </form>
    </div>

    <?php

    echo '<table>';

    if ($stmt->num_rows > 0) {
        $currentCourse = null;

        echo '<tr class="tableHeader3">
                <td>Course Id</td>
                <td>Course Title</td>
                <td>Program Id</td>
                <td colspan="2">Program Title</td>
                <td>Minimum</td>
                <td>Hours</td>
                <td>Program Manager</td>
              </tr>';

        while ($stmt->fetch()) {
            if ($currentCourse != $courseId) {
                echo '<tr class="tableHeader3">
                        <td>'.$courseId.'</td>
                        <td colspan="2">'.$courseTitle.'</td>
                        <td colspan="5"/>
                      </tr>';
                $currentCourse = $courseId;
            }
            echo '<tr>
                    <td colspan="2"/>
                    <td>'.$programId.'</td>
                    <td colspan="2">'.$programTitle.'</td>
                    <td>'.$programMinimum.'</td>
                    <td>'.$hours.'</td>
                    <td>'.$pmFirstName.' '.$pmLastName.'</td>
                  </tr>';

        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('pageFoot.php');