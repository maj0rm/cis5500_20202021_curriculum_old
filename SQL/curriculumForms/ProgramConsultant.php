<?php

/**
 * ProgramConsultant.php
 *
 * Return a list of consultants and their programs
 *
 * @author twhiten
 * @since 20201/03/09
 */

    include('pageHead.php');


    $query = 'SELECT ProgramId, ProgramTitle, concat(PersonFirstName, " ", PersonLastName) AS PersonName FROM LookupCatalogYear INNER JOIN ProgramCatalogYear ON LookupCatalogYear.CatalogYearID = ProgramCatalogYear.CatalogYearId AND LookupCatalogYear.CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) INNER JOIN LookupProgramTitle ON ProgramCatalogYear.ProgramTitleId = LookupProgramTitle.ProgramTitleId INNER JOIN ProgramCatalogYearConsultant ON ProgramCatalogYear.ProgramCatalogYearId = ProgramCatalogYearConsultant.ProgramCatalogYearId INNER JOIN Person ON Person.PersonId = ProgramCatalogYearConsultant.ConsultantPersonId AND Person.Active ORDER BY PersonName, ProgramId';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($programId, $programTitle, $consultant);


    ?>

    <div class="header">

    </div>

    <?php



    if ($stmt->num_rows > 0) {

        $currentConsultant = null;

        echo '<table>
              <tr class="tableHeader">
                <td>Consultant</td>
                <td>Program Code</td>
                <td colspan="2">Program Title</td>
              </tr>';


        while ($stmt->fetch()) {

            if ($currentConsultant != $consultant) {
                echo '<tr><td>'.$consultant.'</td><td colspan="3"/></tr>';
                $currentConsultant = $consultant;
            }

            echo '<tr>
                    <td/>
                    <td>'.$programId.'</td>
                    <td colspan="2">'.$programTitle.'</td>
                  </tr>
';
        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="2">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('pageFoot.php');