<?php

/**
 * Learning Outcome Subreport.php
 *
 * Return a list of learning outcomes and related competencies.
 *
 * @author twhiten
 * @since 20201/03/08
 */


    $subdb = new mysqli('localhost', 'root', '', 'twhiten_curriculum');

    $subquery = 'SELECT LearningOutcomeName, LearningCompetencyName FROM learningoutcome INNER JOIN learningcompetency ON learningoutcome.learningOutcomeId = learningcompetency.learningoutcomeid INNER JOIN CourseCatalogYear ON learningoutcome.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN LookupCatalogYear ON CourseCatalogYear.CatalogYearId = LookupCatalogYear.CatalogYearId';



    if (isset($courseSearch)) {
        $subquery .= ' AND CourseId = ?';
    }
    if (isset($yearSearch)) {
        $subquery .= ' AND CatalogYearName = ?';
    }
    else {
        $subquery .= ' AND CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear)';
    }

    //$subquery .= " AND ChildTitle IS NOT NULL";

    $subquery .= " ORDER BY LearningCompetency.LearningOutcomeId";

    $substmt = $subdb->prepare($subquery);

    if (!isset($yearSearch)) {
        $substmt->bind_param('s', $courseSearch);
    }
    else {
        $substmt->bind_param('ss', $courseSearch, $yearSearch);
    }

    $substmt->execute();
    $substmt->store_result();


    $substmt->bind_result($outcome, $competency);


    if ($substmt->num_rows > 0) {

        $currentOutcome = null;

        while ($substmt->fetch()) {

            if ($outcome != $currentOutcome) {
                echo '<h3>'.$outcome.'</h3>';

                $currentOutcome = $outcome;
            }


            echo '<p>'.$competency.'</p>';

        }
        echo '</table>';

    }
    else {
        echo '<p>None</p>';
    }

