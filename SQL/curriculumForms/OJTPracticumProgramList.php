<?php

/**
 * OJTPracticumCourseList.php
 *
 * Returns a detailed list of OJT courses.
 *
 * @author twhiten
 * @since 20201/03/09
 */

    include('pageHead.php');


    $query = 'SELECT CatalogYearName, CourseId, CourseTitle, Hours, ProgramId, ProgramTitle, Credits, GradeSchemeName, MinimumGrade FROM LookupCatalogYear INNER JOIN CourseCatalogYear ON LookupCatalogYear.CatalogYearID = CourseCatalogYear.CatalogYearId AND LookupCatalogYear.CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) INNER JOIN ProgramCourse ON CourseCatalogYear.CourseCatalogYearId = ProgramCourse.CourseCatalogYearId INNER JOIN ProgramCatalogYear ON ProgramCatalogYear.ProgramCatalogYearId = ProgramCourse.ProgramCatalogYearId INNER JOIN LookupProgramTitle ON LookupProgramTitle.ProgramTitleId = ProgramCatalogYear.ProgramTitleId INNER JOIN LookupGradeScheme ON CourseCatalogYear.GradeSchemeId = LookupGradeScheme.GradeSchemeId WHERE CourseTitle LIKE "%OJT%" OR CourseTitle LIKE "%Practicum%" OR CourseTitle LIKE "%Job%" OR CourseTitle LIKE "%Clinical%" OR CourseTitle LIKE "%Capstone%" ORDER BY ProgramId, CourseId';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($catalogYear, $courseId, $courseTitle, $hours, $programId, $programTitle, $credits, $gradeScheme, $minimumGrade);


    ?>

    <div class="header">

    </div>

    <?php

    echo '<table>';

    if ($stmt->num_rows > 0) {

        $currentProgram = null;


        echo '<tr class="tableHeader3">
                <td>Program Code</td>
                <td>Program Title</td>
                <td colspan="6"></td>
              </tr>
              <tr class="tableHeader3">
                <td/>
                <td>Course Code</td>
                <td colspan="2">Course Title</td>
                <td>Hours</td>
                <td>Credits</td>
                <td>Grade Scheme</td>
                <td>Passing Grade</td>
              </tr>';

        while ($stmt->fetch()) {


            if ($currentProgram != $programId) {
                echo '<tr>
                        <td>'.$programId.'</td>
                        <td>'.$programTitle.'</td>
                        <td colspan="6"/>
                      </tr>';

                $currentProgram = $programId;
            }
                echo '<tr>
                        <td/>
                        <td>'.$courseId.'</td>
                        <td colspan="2">'.$courseTitle.'</td>
                        <td>'.$hours.'</td>
                        <td>'.$credits.'</td>
                        <td>'.$gradeScheme.'</td>
                        <td>'.$minimumGrade.'</td>
                      </tr>';


        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td>'.$error.'</td></tr>';
    }

    echo '</table>';

    include('pageFoot.php');