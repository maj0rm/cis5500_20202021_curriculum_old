<?php

/**
 * ProgramList.php
 *
 * Return a list of programs.
 *
 * @author twhiten
 * @since 20201/03/05
 */

    include('pageHead.php');


    $query = 'SELECT DISTINCT Program.ProgramId, ProgramTitle FROM Program INNER JOIN ProgramCatalogYear ON ProgramCatalogYear.ProgramId = Program.ProgramId  INNER JOIN LookupProgramTitle ON ProgramCatalogYear.ProgramTitleId = LookupProgramTitle.ProgramTitleId';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($programId, $programTitle);


    ?>

    <div class="header">

    </div>

    <?php

    echo '<table>
              <tr class="tableHeader">
                <td>Program Id</td>
                <td>Program Title</td>
              </tr>';

    if ($stmt->num_rows > 0) {


        while ($stmt->fetch()) {
            echo '<tr><td>'.$programId.'</td><td>'.$programTitle.'</td></tr>
';
        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="2">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('pageFoot.php');